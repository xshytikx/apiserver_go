package jwtmanager

import "github.com/xshytikx/apiserver_go/pkg/consterr"

const (
	ErrUserIsNull = consterr.Err("user is null")

	ErrTokenNotFound = consterr.Err("token not found")
	ErrClaimsIsNull  = consterr.Err("claims is null")
)
