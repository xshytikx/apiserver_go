package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/xshytikx/apiserver_go/internal/app"
	"github.com/xshytikx/apiserver_go/internal/controllers"
	"github.com/xshytikx/apiserver_go/internal/model"
	"net/http"
)

type UserController struct{}

func (uc UserController) Create(c *gin.Context) {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	req := request{}
	if err := c.ShouldBindJSON(&req); err != nil {
		controllers.Error(c, http.StatusBadRequest, err)
		return
	}

	u := &model.User{
		Email:    req.Email,
		Password: req.Password,
		Role:     "user",
	}

	if err := app.Store().User().Create(u); err != nil {
		controllers.Error(c, http.StatusUnprocessableEntity, err)
		return
	}

	u.Sanitize()
	controllers.Respond(c, http.StatusCreated, u)
}

func (uc UserController) Login(c *gin.Context) {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	req := &request{}
	if err := c.ShouldBindJSON(&req); err != nil {
		controllers.Error(c, http.StatusBadRequest, err)
		return
	}

	u, err := app.Store().User().FindByEmail(req.Email)
	if err != nil || !u.ComparePassword(req.Password) {
		controllers.Error(c, http.StatusUnauthorized, controllers.ErrIncorrectEmailOrPassword)
		return
	}

	if err := app.Jwt().User().SetToken(c.Writer, u); err != nil {
		controllers.Error(c, http.StatusInternalServerError, err)
		return
	}

	controllers.Respond(c, http.StatusOK, nil)
}
