package main

import (
	"github.com/sirupsen/logrus"
	"github.com/xshytikx/apiserver_go/internal/app"
	"github.com/xshytikx/apiserver_go/internal/router"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			logrus.Error(r)
		}

		if err := app.Close(); err != nil {
			logrus.Error(err)
		}
	}()

	app.Init()

	routerEngine := router.NewRouter()
	if err := app.Serve(routerEngine); err != nil {
		logrus.Panic(err)
	}
}
