package jwtmanager

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/xshytikx/apiserver_go/internal/model"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager/claims"
	"net/http"
)

type EntityJwt interface {
	GetTokenString(r *http.Request) (string, error)
	SignedString(token *jwt.Token) (string, error)
}

type UserJwt interface {
	EntityJwt
	SetToken(w http.ResponseWriter, user *model.User) error
	ParseTokenString(tokenString string) (*claims.UserClaims, *jwt.Token, error)
	RefreshToken(w http.ResponseWriter, claims *claims.UserClaims) error
	CreateToken(user *model.User) (*jwt.Token, error)
}

type ManagerJwt interface {
	User() UserJwt
}
