package sqlstore

import (
	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq" // ...
	"github.com/xshytikx/apiserver_go/internal/store"
)

// Store ...
type Store struct {
	db             *sqlx.DB
	userRepository *UserRepository
}

// New ...
func New(db *sqlx.DB) *Store {
	return &Store{
		db: db,
	}
}

// User ...
func (s *Store) User() store.UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{
		store: s,
	}

	return s.userRepository
}
