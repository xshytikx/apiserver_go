package sqlstore

import (
	"database/sql"
	"github.com/xshytikx/apiserver_go/internal/model"
	"github.com/xshytikx/apiserver_go/internal/store"
)

// UserRepository ...
type UserRepository struct {
	store *Store
}

// Create ...
func (r *UserRepository) Create(u *model.User) error {
	if err := u.Validate(); err != nil {
		return err
	}

	if err := u.BeforeCreate(); err != nil {
		return err
	}

	return r.store.db.QueryRow(
		"INSERT INTO users (email, encrypted_password, role) VALUES($1, $2, $3) RETURNING id",
		u.Email,
		u.EncryptedPassword,
		u.Role,
	).Scan(&u.ID)
}

// Find ...
func (r *UserRepository) Find(id int) (*model.User, error) {
	u := model.User{}

	if err := r.store.db.Get(
		&u,
		"SELECT id, email, encrypted_password, role FROM users WHERE id = $1",
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return &u, nil
}

// FindByEmail ...
func (r *UserRepository) FindByEmail(email string) (*model.User, error) {
	u := model.User{}
	if err := r.store.db.Get(
		&u,
		"SELECT id, email, encrypted_password, role FROM users WHERE email = $1",
		email,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return &u, nil
}
