package claims

import "github.com/dgrijalva/jwt-go"

type UserClaims struct {
	jwt.StandardClaims

	ID   uint
	Role string
}
