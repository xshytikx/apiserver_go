package middlewares_test

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/xshytikx/apiserver_go/internal/app"
	"github.com/xshytikx/apiserver_go/internal/controllers"
	"github.com/xshytikx/apiserver_go/internal/controllers/middlewares"
	"github.com/xshytikx/apiserver_go/internal/model"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	app.SetMode(app.TestMode)
	app.Init()

	os.Exit(m.Run())
}

func TestAccessControl(t *testing.T) {
	u := model.TestUser(t)
	_ = app.Store().User().Create(u)

	testCases := []struct {
		name         string
		cookieValue  map[interface{}]interface{}
		role         string
		path         string
		expectedCode int
	}{
		{
			name:         "foo user",
			path:         "/foo",
			expectedCode: http.StatusOK,
			role:         "user",
		},
		{
			name:         "foo admin",
			path:         "/foo",
			expectedCode: http.StatusOK,
			role:         "admin",
		},
		{
			name:         "bar user",
			path:         "/bar",
			expectedCode: http.StatusOK,
			role:         "user",
		},
		{
			name:         "bar admin",
			path:         "/bar",
			expectedCode: http.StatusOK,
			role:         "admin",
		},
		{
			name:         "sigma user",
			path:         "/sigma",
			expectedCode: http.StatusUnauthorized,
			role:         "user",
		},
		{
			name:         "sigma admin",
			path:         "/sigma",
			expectedCode: http.StatusOK,
			role:         "admin",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			_, r := gin.CreateTestContext(rec)

			r.Use(middlewares.AccessControl())
			r.Use(func(ctx *gin.Context) {
				ctx.Status(http.StatusOK)
			})

			req, _ := http.NewRequest(http.MethodGet, tc.path, nil)
			u.Role = tc.role

			r.ServeHTTP(
				rec, req.WithContext(context.WithValue(req.Context(), controllers.CtxKeyUser, u)),
			)

			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}
}

func TestAuthenticateUser(t *testing.T) {
	u := model.TestUser(t)
	app.Store().User().Create(u)

	token, _ := app.Jwt().User().CreateToken(u)
	authToken, _ := app.Jwt().User().SignedString(token)

	testCases := []struct {
		name         string
		token        string
		expectedCode int
	}{
		{
			name:         "authenticated",
			token:        authToken,
			expectedCode: http.StatusOK,
		},
		{
			name:         "not authenticated",
			token:        "dsfsdfsdfs",
			expectedCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			_, r := gin.CreateTestContext(rec)
			r.Use(middlewares.AuthenticateUser())
			r.Use(func(c *gin.Context) {
				c.Status(http.StatusOK)
			})

			req, _ := http.NewRequest(http.MethodGet, "/", nil)
			cookie := &http.Cookie{
				Name:     jwtmanager.DefaultUserTokenName,
				Value:    tc.token,
				Path:     "/",
				Secure:   false,
				HttpOnly: true,
			}
			req.AddCookie(cookie)

			r.ServeHTTP(rec, req)
			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}

}
