package middlewares

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/xshytikx/apiserver_go/internal/app"
	"github.com/xshytikx/apiserver_go/internal/controllers"
	"github.com/xshytikx/apiserver_go/internal/model"
	"net/http"
)

func AccessControl() gin.HandlerFunc {
	return func(c *gin.Context) {
		u := c.Request.Context().Value(controllers.CtxKeyUser)

		if u == nil {
			controllers.Error(c, http.StatusUnauthorized, controllers.ErrNotAuthenticated)
			return
		}

		if user, ok := u.(*model.User); ok {

			ok, err := app.Enforcer().Enforce(user.Role, c.Request.URL.Path, c.Request.Method)

			if err != nil {
				controllers.Error(c, http.StatusInternalServerError, err)
				return
			}

			if !ok {
				controllers.Error(c, http.StatusUnauthorized, controllers.ErrNotAuthenticated)
				return
			}

			c.Next()
			return
		}

		controllers.Error(c, http.StatusUnauthorized, controllers.ErrNotAuthenticated)
	}
}

func AuthenticateUser() gin.HandlerFunc {
	return func(c *gin.Context) {

		tokenString, err := app.Jwt().User().GetTokenString(c.Request)
		if err != nil {
			controllers.Error(c, http.StatusInternalServerError, err)
		}

		claims, token, err := app.Jwt().User().ParseTokenString(tokenString)
		if err != nil {
			controllers.Error(c, http.StatusInternalServerError, err)
			return
		}

		var u interface{} = nil
		if token.Valid {
			u, err = app.Store().User().Find(int(claims.ID))
			if err != nil {
				controllers.Error(c, http.StatusUnauthorized, controllers.ErrNotAuthenticated)
				return
			}

			if err := app.Jwt().User().RefreshToken(c.Writer, claims); err != nil {
				controllers.Error(c, http.StatusInternalServerError, err)
				return
			}
		}
		c.Request = c.Request.WithContext(context.WithValue(c.Request.Context(), controllers.CtxKeyUser, u))

		c.Next()
	}
}
