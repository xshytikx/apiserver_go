package app

const (
	EnvAppAddr     = "APP_ADDR"
	EnvDbUrl       = "DB_URL"
	EnvDbTableRule = "DB_TABLE_RULE"
	EnvAuthModel   = "AUTH_MODEL"
	EnvJwtKet      = "JWT_KEY"
)
