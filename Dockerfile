# STEP 1
FROM golang:alpine AS builder

RUN mkdir /app
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/app ./cmd/apiserver

# STEP 2
FROM alpine
COPY --from=builder /go/bin/app /go/bin/app
ENTRYPOINT ["/go/bin/app"]