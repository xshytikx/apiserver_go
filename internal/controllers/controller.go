package controllers

import (
	"github.com/gin-gonic/gin"
)

func Error(c *gin.Context, code int, err error) {
	Respond(c, code, gin.H{"error": err.Error()})

	c.Abort()
}

func Respond(c *gin.Context, code int, data interface{}) {
	c.JSON(code, data)
}
