package app

import "github.com/xshytikx/apiserver_go/pkg/consterr"

const (
	ErrAppNotInit = consterr.Err("app not init")
)
