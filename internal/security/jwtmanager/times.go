package jwtmanager

import "time"

const (
	DefaultExpires         = time.Duration(86400*30) * time.Second
	DefaultRefreshInterval = DefaultExpires - 24*time.Hour
)
