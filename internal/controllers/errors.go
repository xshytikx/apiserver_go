package controllers

import "github.com/xshytikx/apiserver_go/pkg/consterr"

const (
	ErrIncorrectEmailOrPassword = consterr.Err("incorrect email or password")
	ErrNotAuthenticated         = consterr.Err("not authenticated")
)
