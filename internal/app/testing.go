package app

import (
	"github.com/casbin/casbin/v2"
	casmodel "github.com/casbin/casbin/v2/model"
	"github.com/sirupsen/logrus"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager"
	"github.com/xshytikx/apiserver_go/internal/store/teststore"
)

func testInit() {
	once.Do(func() {
		instance = &app{
			logger:     logrus.New(),
			store:      teststore.New(),
			jwtManager: jwtmanager.New("secret"),
			enforcer:   testEnforcer(),
		}
	})
}

// testEnforcer ...
func testEnforcer() *casbin.SyncedEnforcer {
	text :=
		`
		[request_definition]
		r = sub, obj, act
		
		[policy_definition]
		p = sub, obj, act
		
		[role_definition]
		g = _, _
		
		[policy_effect]
		e = some(where (p.eft == allow))
		
		[matchers]
		m = g(r.sub, p.sub) && (r.obj == p.obj || p.obj == "/*") && (r.act == p.act || p.act == "*")
	`

	adapter, err := casmodel.NewModelFromString(text)
	if err != nil {
		logrus.Fatal(err)
		return nil
	}

	enforcer, err := casbin.NewSyncedEnforcer(adapter)
	if err != nil {
		logrus.Fatal(err)
		return nil
	}

	if err := testSetPolicy(enforcer); err != nil {
		logrus.Fatal(err)
		return nil
	}

	return enforcer
}

func testSetPolicy(enforcer *casbin.SyncedEnforcer) error {
	policies := [][]string{
		{"admin", "/*", "*"},
		{"register", "/register", "POST"},
		{"login", "/login", "POST"},
		{"general", "/whoami", "GET"},
		{"user", "/foo", "GET"},
		{"user", "/bar", "GET"},
	}
	for p := range policies {
		if _, err := enforcer.AddPolicy(policies[p]); err != nil {
			logrus.Fatal(err)
			return err
		}
	}

	group := [][]string{
		{"guest", "login"},
		{"guest", "general"},
		{"guest", "register"},
		{"user", "general"},
	}
	for g := range group {
		if _, err := enforcer.AddGroupingPolicy(group[g]); err != nil {
			logrus.Fatal(err)
			return err
		}
	}

	return nil
}
