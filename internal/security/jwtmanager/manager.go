package jwtmanager

type manager struct {
	key  []byte
	user *user
}

func New(key string) ManagerJwt {
	return &manager{
		key: []byte(key),
	}
}

func (m *manager) User() UserJwt {
	if m.user == nil {
		m.user = newUser(m)
	}

	return m.user
}
