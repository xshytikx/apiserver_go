package jwtmanager

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/xshytikx/apiserver_go/internal/model"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager/claims"
	"net/http"
	"net/url"
	"time"
)

func newUser(m *manager) *user {
	return &user{
		manager:         m,
		key:             m.key,
		tokenName:       DefaultUserTokenName,
		expires:         DefaultExpires,
		refreshInterval: DefaultRefreshInterval,
	}
}

type user struct {
	key             []byte
	tokenName       string
	manager         *manager
	expires         time.Duration
	refreshInterval time.Duration
}

func (u *user) SignedString(token *jwt.Token) (string, error) {
	if token != nil {
		return token.SignedString(u.key)
	}

	return "", errors.New("token is null")
}

func (u *user) CreateToken(user *model.User) (*jwt.Token, error) {
	if user != nil {
		return jwt.NewWithClaims(jwt.SigningMethodHS256, &claims.UserClaims{
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: time.Now().Add(u.expires).Unix(),
			},
			ID:   uint(user.ID),
			Role: user.Role,
		}), nil
	}

	return nil, ErrUserIsNull
}

func (u *user) RefreshToken(w http.ResponseWriter, claims *claims.UserClaims) error {
	if claims != nil {

		if w != nil {

			if time.Until(time.Unix(claims.ExpiresAt, 0)) < u.refreshInterval {
				expires := time.Now().Add(u.expires)
				claims.ExpiresAt = expires.Unix()
				token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

				tokenString, err := token.SignedString(u.key)
				if err != nil {
					return err
				}

				http.SetCookie(w, &http.Cookie{
					Name:     u.tokenName,
					Value:    tokenString,
					Expires:  expires,
					Path:     "/",
					Secure:   false,
					HttpOnly: true,
				})
			}

			return nil
		}

		return http.ErrBodyNotAllowed
	}

	return ErrClaimsIsNull
}

func (u *user) ParseTokenString(tokenString string) (*claims.UserClaims, *jwt.Token, error) {
	if tokenString != "" {
		c := &claims.UserClaims{}

		token, err := jwt.ParseWithClaims(tokenString, c, func(token *jwt.Token) (interface{}, error) {
			return u.key, nil
		})
		if err != nil {
			return nil, nil, err
		}

		return c, token, nil
	}

	return nil, nil, ErrTokenNotFound
}

func (u *user) GetTokenString(r *http.Request) (string, error) {
	cookie, err := r.Cookie(u.tokenName)
	if err != nil {
		return "", err
	}
	tokenString, _ := url.QueryUnescape(cookie.Value)

	return tokenString, nil
}

func (u *user) SetToken(w http.ResponseWriter, user *model.User) error {
	if user != nil {

		if w != nil {

			token, err := u.CreateToken(user)
			if err != nil {
				return err
			}

			tokenString, err := u.SignedString(token)
			if err != nil {
				return err
			}

			http.SetCookie(w, &http.Cookie{
				Name:     u.tokenName,
				Value:    tokenString,
				Expires:  time.Now().Add(u.expires),
				Path:     "/",
				Secure:   false,
				HttpOnly: true,
			})

			return nil
		}

		return http.ErrBodyNotAllowed
	}

	return ErrUserIsNull
}
