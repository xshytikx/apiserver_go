package store

import "github.com/xshytikx/apiserver_go/pkg/consterr"

const (
	// ErrRecordNotFound ...
	ErrRecordNotFound = consterr.Err("record not found")
)
