package router

import (
	"github.com/gin-gonic/gin"
	ginlogrus "github.com/toorop/gin-logrus"
	"github.com/xshytikx/apiserver_go/internal/app"
	"github.com/xshytikx/apiserver_go/internal/controllers/middlewares"
	v1 "github.com/xshytikx/apiserver_go/internal/controllers/v1"
)

func NewRouter() *gin.Engine {
	router := gin.New()
	router.Use(ginlogrus.Logger(app.Logger()), gin.Recovery())

	api := router.Group("api")
	mapV1Routes(api)

	return router
}

func mapV1Routes(router *gin.RouterGroup) {
	v1Group := router.Group("v1")
	{
		userGroup := v1Group.Group("users")
		{
			user := new(v1.UserController)
			userGroup.POST("/register", user.Create)
			userGroup.POST("/login", user.Login)
		}
		testingGroup := v1Group.Group("testing")
		{
			testing := new(v1.TestingController)
			testingGroup.Use(middlewares.AuthenticateUser())
			testingGroup.Use(middlewares.AccessControl())
			testingGroup.GET("/whoami", testing.WhoAmI)
			testingGroup.GET("/simple", testing.Simple)
		}
	}
}
