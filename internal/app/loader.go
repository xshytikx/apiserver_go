package app

import (
	sqlxadapter "github.com/Blank-Xu/sqlx-adapter"
	"github.com/casbin/casbin/v2"
	casmodel "github.com/casbin/casbin/v2/model"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager"
	"time"
)

func initLogger(level string) *logrus.Logger {
	logger := logrus.New()

	logLevel, err := logrus.ParseLevel(level)
	if err != nil {
		panic(err)
	}

	logger.SetLevel(logLevel)
	logger.SetFormatter(&logrus.TextFormatter{
		DisableQuote: true,
		ForceColors:  true,
	})

	return logger
}

func initDB() *sqlx.DB {
	db, err := sqlx.Connect("postgres", viper.GetString(EnvDbUrl))
	if err != nil {
		panic(err)
	}

	return db
}

func initEnforcer(db *sqlx.DB) *casbin.SyncedEnforcer {
	a, err := sqlxadapter.NewAdapter(db, viper.GetString(EnvDbTableRule))
	if err != nil {
		panic(err)
	}

	authModel, err := casmodel.NewModelFromString(viper.GetString(EnvAuthModel))
	if err != nil {
		panic(err)
	}

	enforcer, err := casbin.NewSyncedEnforcer(authModel, a)
	if err != nil {
		panic(err)
	}

	enforcer.StartAutoLoadPolicy(time.Second * 5)

	return enforcer
}

func initJwtManager() jwtmanager.ManagerJwt {
	return jwtmanager.New(viper.GetString(EnvJwtKet))
}
