package app

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/xshytikx/apiserver_go/internal/security/jwtmanager"
	"github.com/xshytikx/apiserver_go/internal/store"
	"github.com/xshytikx/apiserver_go/internal/store/sqlstore"
	"net/http"
	"sync"
	"time"
)

func init() {
	if err := godotenv.Load(); err != nil {
		logrus.Info(err)
	}

	viper.AutomaticEnv()
}

type app struct {
	logger     *logrus.Logger
	db         *sqlx.DB
	store      store.Store
	jwtManager jwtmanager.ManagerJwt
	enforcer   *casbin.SyncedEnforcer
}

const (
	DebugMode   = "debug"
	ReleaseMode = "release"
	TestMode    = "test"
)

const (
	debugCode = iota
	releaseCode
	testCode
)

var (
	mode     int
	modeName string
	once     sync.Once
	instance *app
)

func SetMode(value string) {
	switch value {
	case DebugMode, "":
		mode = debugCode
	case ReleaseMode:
		mode = releaseCode
	case TestMode:
		mode = testCode
	default:
		panic("app mode unknown: " + value)
	}
	modeName = value
}

func Init() {
	defer func() {
		if r := recover(); r != nil {
			logrus.Panic(r)
		}
	}()
	SetMode(modeName)

	switch mode {
	case testCode:
		testInit()
	case debugCode:
		debugInit()
	}

}

func debugInit() {
	db := initDB()

	once.Do(func() {
		instance = &app{
			logger:     initLogger(logrus.DebugLevel.String()),
			db:         db,
			store:      sqlstore.New(db),
			jwtManager: initJwtManager(),
			enforcer:   initEnforcer(db),
		}
	})
}

func Serve(r *gin.Engine) error {
	if Get() == nil {
		return ErrAppNotInit
	}

	s := &http.Server{
		Addr:           viper.GetString(EnvAppAddr),
		Handler:        r,
		ReadTimeout:    time.Second * 10,
		WriteTimeout:   time.Second * 10,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		return err
	}

	return nil
}

func Get() *app {
	return instance
}

func (a *app) Store() store.Store {
	return a.store
}

func (a *app) Logger() *logrus.Logger {
	return a.logger
}

func (a *app) Enforcer() *casbin.SyncedEnforcer {
	return a.enforcer
}

func (a *app) Jwt() jwtmanager.ManagerJwt {
	return a.jwtManager
}

func Jwt() jwtmanager.ManagerJwt {
	return Get().Jwt()
}

func Enforcer() *casbin.SyncedEnforcer {
	return Get().Enforcer()
}

func Logger() *logrus.Logger {
	return Get().Logger()
}

func Store() store.Store {
	return Get().Store()
}

func Close() error {
	if a := Get(); a != nil {
		return a.db.Close()
	}

	return nil
}
