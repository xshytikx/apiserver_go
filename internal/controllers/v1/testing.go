package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/xshytikx/apiserver_go/internal/controllers"
	"net/http"
)

type TestingController struct{}

func (tc TestingController) Simple(c *gin.Context) {
	controllers.Respond(c, http.StatusOK, c.Request.URL.Path)
}

func (tc TestingController) WhoAmI(c *gin.Context) {
	controllers.Respond(c, http.StatusOK, c.Request.Context().Value(controllers.CtxKeyUser))
}
